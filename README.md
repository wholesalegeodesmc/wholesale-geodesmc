**Michal & Company Wholesale Geodes**

We incorporate the use of natural crystals, gems, minerals, and fossils Nature’s Art™ into geode décor accents for your home creating unique custom geode furniture, Nature’s Art Decor™ for clients that desire and collect one-of-a-kind artifacts, at Michal& Co, we are a direct importers and wholesalers company with design and manufacturing facilities, we have the ability to provide geode home decor products in various quantities to retail stores.
[https://michalandcompany.com/](https://michalandcompany.com/)

---

